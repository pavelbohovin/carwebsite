<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css' );
}

// Car image sizes
add_image_size( '142x97', 142, 97, true );
add_image_size( '262x160', 262, 160, true );
add_image_size( '297x180', 297, 180, true );
add_image_size( '350x233', 350, 233, true );
add_image_size( '360x240', 360, 240, true );
add_image_size( '750x502', 750, 502, true );
