<?php get_header(); ?>
<?php get_template_part('template_inc/inc','menu'); ?>
<?php get_template_part('template_inc/inc','title-breadcrumb');

$autoshowroom_portfolio_description_limit = ot_get_option('autoshowroom_TZVehicle_limit');
$autoshowroom_portfolio_specifications_arr = ot_get_option('autoshowroom_TZVehicle_specs');
$autoshowroom_portfolio_sidebar = ot_get_option('autoshowroom_TZVehicle_sidebar');
$autoshowroom_vehicle_excerpt = ot_get_option('autoshowroom_TZVehicle_excerpt',1);
$showcompare    = ot_get_option('autoshowroom_Detail_show_compare','yes');
$autoshowroom_vehicle_total = $wp_query->found_posts;

if($autoshowroom_portfolio_sidebar=='none'){
	$autoshowroom_columns = 12;
} else{
	$autoshowroom_columns = 9;
}
$desktopcolumns = ot_get_option('autoshowroom_TZVehicle_desktop_col');
$tabletcolumns = ot_get_option('autoshowroom_TZVehicle_tablet_col');
$mobilecolumns = ot_get_option('autoshowroom_TZVehicle_mobile_col');

wp_enqueue_script('autoshowroom-masonry.pkgd');
wp_enqueue_script('autoshowroom-imagesloaded.pkgd');
wp_enqueue_script('autoshowroom-masonry');
wp_enqueue_script('autoshowroom-vehicle-masonry');
?>
	<section class="container-content default-page vehicle-page-masonry"
	         data-desktop="<?php echo esc_attr($desktopcolumns);?>"
	         data-tablet="<?php echo esc_attr($tabletcolumns);?>"
	         data-mobile="<?php echo esc_attr($mobilecolumns);?>">
		<div class="container">
			<div class="row">
				<?php if($autoshowroom_portfolio_sidebar=='left'){ ?>
					<div class="col-md-3 tz-sidebar tz-sidebar-shop autoshowroom-sidebar">
						<?php
						if(is_active_sidebar("autoshowroom-sidebar-inventory")):
							dynamic_sidebar("autoshowroom-sidebar-inventory");
						endif;
						?>
					</div>
				<?php } ?>
				<div class="col-md-<?php echo esc_attr($autoshowroom_columns);?>">
					<div class="row">
						<div class="col-md-12">
							<div class="vehicle-results">
								<span class="results-text"><?php esc_html_e('Your search returned ','autoshowroom'); echo esc_attr($autoshowroom_vehicle_total); esc_html_e(' results','autoshowroom'); ?></span>
								<div class="vehicle-layouts">
									<?php esc_html_e('View as: ','autoshowroom');?>
									<a href="javascript: " class="vehicle-layout-grid-button"><i class="fa fa-th"></i>
										<span class="tooltip-content"><?php esc_html_e('View Grid','autoshowroom');?></span>
									</a>
									<a href="javascript: " class="vehicle-layout-list-button"><i class="fa fa-th-list"></i>
										<span class="tooltip-content"><?php esc_html_e('View List','autoshowroom');?></span>
									</a>
								</div>
								<div class="clr"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="vehicle-masonry">
							<?php
							while (have_posts()) : the_post() ;
								?>
								<div class="vehicle-grid">
									<div class="TZ-Vehicle-Grid">
										<div class="item">
											<div class="Vehicle-Feature-Image">
												<a href="<?php the_permalink(); ?>" style="background-image: url(<?php the_post_thumbnail_url( '297x180'); ?>);"></a>
											</div>
											<h4 class="Vehicle-Title">
												<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											</h4>
											<?php
											if($autoshowroom_vehicle_excerpt == 1){
												if($autoshowroom_portfolio_description_limit){
													$desc = substr(strip_tags(get_the_excerpt()), 0, $autoshowroom_portfolio_description_limit);
													?>
													<div class="vehicle-feature-des">
														<p><?php echo esc_attr($desc);?></p>
													</div>
												<?php } else{
													echo get_the_excerpt();
												}
											}
											?>
											<?php echo balanceTags(tz_autoshowroom_get_vehicle_specs(get_the_ID(),$autoshowroom_portfolio_specifications_arr));?>
											<?php echo balanceTags(tz_autoshowroom_filter_vehicle_price(get_the_ID())); ?>
											<div class="vehicle-btn">
												<?php if($showcompare=='yes'){ ?>
													<span class="btn-function btn_detail_compare" data-text="<?php esc_html_e('In Compare List','autoshowroom');?>"
													      data-id="<?php echo esc_attr(get_the_ID());?>">
                                            <i class="fa fa-car"></i>
														<?php esc_html_e('Add to Compare','autoshowroom');?>
                                        </span>
												<?php } ?>

												<a href="<?php the_permalink(); ?>">
													<i class="fa fa-arrow-circle-right"></i>
													<?php esc_html_e('View More','autoshowroom'); ?>
												</a>
											</div>
											<div class="clr"></div>
										</div>
									</div>
								</div>
								<?php
								comments_template();
							endwhile;
							?>
						</div>
					</div>
					<?php
					if ( function_exists('wp_pagenavi') ):
						wp_pagenavi();
					else:
						tz_autoshowroom_content_nav('bottom-nav');
					endif;
					?>
				</div>
				<?php if($autoshowroom_portfolio_sidebar=='right'){ ?>
					<div class="col-md-3 tz-sidebar tz-sidebar-shop autoshowroom-sidebar">
						<?php
						if(is_active_sidebar("autoshowroom-sidebar-inventory")):
							dynamic_sidebar("autoshowroom-sidebar-inventory");
						endif;
						?>
					</div>
				<?php } ?>
			</div>

		</div>

	</section>
<?php
get_template_part('template_inc/inc','footer');
get_footer();
?>