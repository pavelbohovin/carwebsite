<?php get_header(); ?>
<?php get_template_part('template_inc/inc','menu'); ?>
<?php get_template_part('template_inc/inc','title-breadcrumb');

$symbol 		= get_option( 'options_pcd_currency_symbol', '$' );
$txttaxes 		= ot_get_option('autoshowroom_Detail_tax_txt');
$txtrelated 	= ot_get_option('autoshowroom_Detail_related_txt','Related Cars');

$autoshowroom_TZVehicleCalculator_on    = ot_get_option('autoshowroom_TZVehicleCalculator_on');
$autoshowroom_TZVehicleCalculator_Title = ot_get_option('autoshowroom_TZVehicleCalculator_Title');
$autoshowroom_TZVehicleCalculator_down  = ot_get_option('autoshowroom_TZVehicleCalculator_down');
$autoshowroom_TZVehicleCalculator_months = ot_get_option('autoshowroom_TZVehicleCalculator_down_months');
$autoshowroom_portfolio_description_limit = ot_get_option('autoshowroom_TZVehicle_limit');
$autoshowroom_portfolio_specifications_arr = ot_get_option('autoshowroom_TZVehicle_specs');

$autoshowroom_detail_popup = ot_get_option('autoshowroom_Detail_show_popup','yes');

$autoshowroom_related_show = ot_get_option('autoshowroom_Detail_show_related');
$autoshowroom_related_number = ot_get_option('autoshowroom_Detail_related_number','4');

/* Art - Get custom field */
$fields_to_show = array( 'Registration date' => 'registration',
					     'Mileage' => 'milage',
						 'Condition' => 'condition',
						 'Exterior Color' => 'color',
						 'Interior Color' => 'interior',
						 'Transmission' => 'transmission',
						 'Engine' => 'engine',
						 'Drivetrain' => 'drivetrain',
						 'Year' => 'year',
						 'Upholstery' => 'upholstery',
						 'Propellant' => 'propellant' );

$txtprice       = ot_get_option('autoshowroom_TZVehicleCalculator_price_txt');
$txtdownpay     = ot_get_option('autoshowroom_TZVehicleCalculator_d_txt');
$txtrate        = ot_get_option('autoshowroom_TZVehicleCalculator_rate_txt');
$txtPeriod      = ot_get_option('autoshowroom_TZVehicleCalculator_loan_txt');
$txtbtn         = ot_get_option('autoshowroom_TZVehicleCalculator_btn_txt');

$txtmonty       = ot_get_option('autoshowroom_TZVehicleCalculator_monthy_txt');
$txtannual      = ot_get_option('autoshowroom_TZVehicleCalculator_annual_txt');
$txttotal       = ot_get_option('autoshowroom_TZVehicleCalculator_total_txt');

$showcompare    = ot_get_option('autoshowroom_Detail_show_compare','yes');
$showbrochure   = ot_get_option('autoshowroom_Detail_show_brochure','yes');
if($autoshowroom_detail_popup=='yes'){
    wp_enqueue_style('autoshowroom-lightgallery-css');
    wp_enqueue_script('autoshowroom-lightgallery');
    wp_enqueue_script('autoshowroom-mousewheel');
}
?>
    <section class="container-content default-page vehicle-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="vehicle-title"><?php the_title();?></h1>
                    <div class="vehicle-btn-function">
                        <?php if($showcompare=='yes'){ ?>
                        <span class="btn-function btn_detail_compare" data-images="<?php the_post_thumbnail_url( 'medium' );?>"
                              data-id="<?php echo esc_attr(get_the_ID());?>" data-text="<?php esc_html_e('In Compare List','autoshowroom');?>">
                            <i class="fa fa-car"></i>
                            <?php esc_html_e('Add to Compare','autoshowroom');?>
                        </span>
                        <?php } ?>
                        <?php if($showbrochure=='yes'){ ?>
                        <a href="<?php echo esc_url(get_post_meta( get_the_ID(), 'autoshowroom_vehicle_brochure', true ));?>">
                            <span class="btn-function"><i class="fa fa-file-pdf-o"></i> <?php esc_html_e('Car Brochure','autoshowroom');?></span>
                        </a>
                        <?php } ?>
                    </div>
                    <div id="slider" class="flexslider">
                        <ul class="slides cardeal_slide">
                            <?php
								$autoshowroom_images = get_field( 'images' );
								if( !empty($autoshowroom_images) ) {
									if ( $autoshowroom_images = get_field( 'images' ) ) {
										foreach ($autoshowroom_images as $image) { ?>
											<li data-src="<?php echo esc_url($image['url']);?>">
												<a><img src="<?php echo esc_url($image['url']);?>" alt="<?php echo esc_attr($image['title']);?>"/></a>
											</li>
										<?php }
									}
								} else {
									$m_image = get_attached_media( 'image' );
									if( !empty($m_image) ) {
										foreach ($m_image as $image) { 
										?>
											<li data-src="<?php echo $image->guid;?>">
												<img src="<?php echo wp_get_attachment_image_url( $image->ID, '750x502' );?>" alt=""/>
											</li>
										<?php 
										}					
									}
								}	
                            ?>
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider">
                        <ul class="slides">
                            <?php
							if( !empty($autoshowroom_images) ) {
								if ( $autoshowroom_images = get_field( 'images' ) ) {
									foreach ($autoshowroom_images as $image) { ?>
										<li>
											<img src="<?php echo esc_url($image['sizes']['medium']);?>" alt="<?php echo esc_attr($image['title']);?>"/>
										</li>
									<?php }
								}
                            } else {
								$m_image = get_attached_media( 'image' );
								if( !empty($m_image) ) {
									foreach ($m_image as $image) { 
									?>
										<li>
											<img src="<?php echo wp_get_attachment_image_url( $image->ID, '142x97' );?>" alt=""/>
										</li>
									<?php 
									}					
								}
							}							
							?>
                        </ul>
                    </div>
                    <div class="vehicle-content">
						<?php 
						
							$list = explode(",", get_field( 'equipmentlist' ) );
							$out = '<div ="eq-list">';
							$outL = $outR = '';
							$count = 0;
							foreach($list as $value) {
								if($value) {
									$count++;
									if( $count % 2 == 0 ) {
										$outR .= '<div class="su-list su-list-style-">'.$value.'</div>';
									} else {
										$outL .= '<div class="su-list su-list-style-">'.$value.'</div>';
									}
								}
							}
								
							$out .= '</div>';
							
							echo do_shortcode( '[su_tabs]
								[su_tab title="Vehicle Overview"]
									[vehicle_description]</br></br>
								[/su_tab]
								[su_tab title="Features & Options"]
									<h3>'.get_the_title().'</h3>
									[su_row]
										[su_column size="1/2"]
											<h3> General </h3><ol>'.											
											'<li>Variant<strong>'.get_field( 'variant' ).'</strong></li>'.
											'<li>TotalWeight<strong>'.get_field( 'totalweight' ).'</strong></li>'.
											'<li>Length<strong>'.get_field( 'length' ).'</strong></li>'.
											'<li>Width<strong>'.get_field( 'width' ).'</strong></li>'.
											'<li>Height<strong>'.get_field( 'height' ).'</strong></li>'.
											'<li>BodyType<strong>'.get_field( 'bodytype' ).'</strong></li>'.
											'<li>NumberOfAirbags<strong>'.get_field( 'numberofairbags' ).'</strong></li>'.
											'<li>NumberOfDoors<strong>'.get_field( 'numberofdoors' ).'</strong></li>'.
											'<li>NumberOfGears<strong>'.get_field( 'numberofgears' ).'</strong></li>'.
											'<li>TrailerWeight<strong>'.get_field( 'trailerweight' ).'</strong></li>'
											.'</ol>
										[/su_column]
										[su_column size="1/2"]
											<h3> Engine </h3><ol>'.											
											'<li>MotorVolume<strong>'.get_field( 'motorvolume' ).'</strong></li>'.
											'<li>Effect<strong>'.get_field( 'effect' ).'</strong></li>'.
											'<li>Cylinders<strong>'.get_field( 'cylinders' ).'</strong></li>'.
											'<li>ValvesPerCylinder<strong>'.get_field( 'valvespercylinder' ).'</strong></li>'.
											'<li>GasTankMax<strong>'.get_field( 'gastankmax' ).'</strong></li>'.
											'<li>KmPerLiter<strong>'.get_field( 'kmperliter' ).'</strong></li>'.
											'<li>Acceleration0To100<strong>'.get_field( 'acceleration0to100' ).'</strong></li>'.
											'<li>TopSpeed<strong>'.get_field( 'topspeed' ).'</strong></li>'.
											'<li>EffectInNm<strong>'.get_field( 'effectinnm' ).'</strong></li>'.
											'<li>EffectInNmRpm<strong>'.get_field( 'effectinnmrpm' ).'</strong></li>'.
											'<li>Weight<strong>'.get_field( 'weight' ).'</strong></li>'.
											'<li>WeightTax<strong>'.get_field( 'weighttax' ).'</strong></li>'.
											'<li>Payload<strong>'.get_field( 'payload' ).'</strong></li>'			
											.'</ol>
										[/su_column]
									[/su_row]
								[/su_tab]
								[su_tab title="Equipment List"]
									[su_row]
										[su_column size="1/2"]<div ="eq-list">'.$outL.'</div>[/su_column]
										[su_column size="1/2"]<div ="eq-list">'.$outR.'</div>[/su_column]
									[/su_row]
								[/su_tab]
								[su_tab title="Request information"]
									[su_gmap address=" 1011 L St NW Washington DC 20001 Hoa Ky "]
									[contact-form-7 id="1176" title="Request information"]
								[/su_tab]
								[/su_tabs]' );
						?>
						<?php
                          //  echo do_shortcode('[vehicle_description]');
                        ?>
                        <div class="autoshowroom-comment-content">
                            <?php comments_template( '', true ); ?>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 autoshowroom-sidebar">
                    <?php echo balanceTags(tz_autoshowroom_filter_vehicle_price(get_the_ID())); ?>
                    <p class="center"><?php echo esc_html($txttaxes);?></p>
                    <div class="vehicle-box">
                        <h3 class="widget-title"><span><?php esc_html_e('Specifications','autoshowroom');?></span></h3>
                        <div class="pcd-specs">
                            <div>
                                <label><?php esc_html_e('Make','autoshowroom');?></label>
                                <span>
                                    <?php $autoshowroom_vehicle_make = wp_get_post_terms(get_the_ID(),'make');
                                    foreach($autoshowroom_vehicle_make as $make){
                                        echo esc_attr($make->name);
                                    }
                                    ?>
                                </span>
                            </div>
                            <div>
                                <label><?php esc_html_e('Model','autoshowroom');?></label>
                                <span>
                                    <?php $autoshowroom_vehicle_make = wp_get_post_terms(get_the_ID(),'model');
                                    foreach($autoshowroom_vehicle_make as $make){
                                        echo esc_attr($make->name);
                                    }
                                    ?>
                                </span>
                            </div>
							<?php foreach($fields_to_show as $field => $value) { 
									$temp_v = get_field( $value );
									if( isset( $temp_v ) and $temp_v != '' ) : ?>
										<div>
											<label><?php echo $field; ?></label>
											<span><?php echo $temp_v; ?></span>
										</div>
									<?php
									endif;
								}
							?>
                            <div class="clr"></div>
                        </div>
                        <?php 
							//echo balanceTags(tz_autoshowroom_get_vehicle_specs(get_the_ID(),$fields_to_show));
						?>
                    </div>
                    <?php  if ( is_active_sidebar( "vehicle-bottom-right" ) ) : ?>
                        <?php dynamic_sidebar( "vehicle-bottom-right" ); ?>
                    <?php endif; ?>
                    <?php
                    if($autoshowroom_TZVehicleCalculator_on=='on'){
                    ?>
                        <div class="vehicle-box">
                            <h3 class="widget-title"><span><?php echo esc_html($autoshowroom_TZVehicleCalculator_Title);?></span></h3>
                            <div class="payment-calculator">
                                <label><?php echo balanceTags($txtprice);?></label>
                                <input class="payment-price" type="text" value="" readonly/>
                                <label><?php echo balanceTags($txtdownpay);?></label>
                                <select class="down-pay-percent">
                                    <?php
                                    foreach($autoshowroom_TZVehicleCalculator_down as $autoshowroom_downpay){?>
                                        <option value="<?php echo esc_attr($autoshowroom_downpay["autoshowroom_TZVehicleCalculator_down_payment"]);?>"
                                            data-input="<?php echo esc_attr($autoshowroom_downpay["autoshowroom_TZVehicleCalculator_Annual"]);?>"><?php echo esc_attr($autoshowroom_downpay["autoshowroom_TZVehicleCalculator_down_payment"]);?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <label><?php echo balanceTags($txtrate);?></label>

                                <input type="text"  value="" class="pay_rate"/>

                                <label><?php echo balanceTags($txtPeriod);?></label>
                                <select class="down-pay-years">
                                    <?php
                                    foreach($autoshowroom_TZVehicleCalculator_months as $autoshowroom_down){?>
                                        <option value="<?php echo esc_attr($autoshowroom_down["autoshowroom_TZVehicleCalculator_down_payment_month"]);?>"><?php echo esc_attr($autoshowroom_down["autoshowroom_TZVehicleCalculator_down_payment_month"]);?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                                <button class="payment_cal_btn" value=""><?php echo esc_attr($txtbtn);?></button>

                            </div>
                            <div class="payment_result">
                                <span><?php echo esc_html($txtmonty);?></span>
                                <span class="strong"><?php echo esc_attr($symbol);?><strong class="monthy"></strong></span>
                                <span><?php echo esc_html($txtannual);?></span>
                                <span class="strong"><?php echo esc_attr($symbol);?><strong class="free-amount"></strong></span>
                                <span><?php echo esc_html($txttotal);?></span>
                                <span class="strong"><?php echo esc_attr($symbol);?><strong class="total-pay"></strong></span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if($autoshowroom_related_show == 'yes'){
                ?>
            <div class="related-container">
                <h3> <?php echo $txtrelated;?></h3>
                <div class="row">
                <?php
                global $post;
                $number = 12/$autoshowroom_related_number;
                $related = get_posts( array( 'post_type'=> 'vehicle', 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => $autoshowroom_related_number, 'post__not_in' => array($post->ID) ) );
                if( $related ) foreach( $related as $post ) {
                    setup_postdata($post); ?>
                    <div class="vehicle-grid col-md-<?php echo $number; ?>">
                        <div class="TZ-Vehicle-Grid">
                            <div class="item">
                                <div class="Vehicle-Feature-Image">
	                                <a href="<?php the_permalink(); ?>" style="background-image: url(<?php the_post_thumbnail_url( '297x180' ); ?>);"></a>
                                </div>
                                <h4 class="Vehicle-Title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h4>
                                <?php if($autoshowroom_portfolio_description_limit){
                                    $desc = substr(strip_tags(do_shortcode('[vehicle_description]')), 0, $autoshowroom_portfolio_description_limit);
                                    ?>
                                    <div class="vehicle-feature-des">
                                        <p><?php echo esc_attr($desc);?></p>
                                    </div>
                                <?php } else{
                                    echo do_shortcode('[vehicle_description]');
                                } ?>

                                <?php echo balanceTags(tz_autoshowroom_get_vehicle_specs(get_the_ID(),$autoshowroom_portfolio_specifications_arr));?>
                                <?php echo balanceTags(tz_autoshowroom_filter_vehicle_price(get_the_ID())); ?>
                                <div class="vehicle-btn">
                                    <span class="btn-function btn_detail_compare" data-text="<?php esc_html_e('In Compare List','autoshowroom');?>"
                                          data-id="<?php echo esc_attr(get_the_ID());?>">
                                        <i class="fa fa-car"></i>
                                        <?php esc_html_e('Add to Compare','autoshowroom');?>
                                    </span>
                                    <a href="<?php the_permalink(); ?>">
                                        <i class="fa fa-arrow-circle-right"></i>
                                        <?php esc_html_e('View More','autoshowroom'); ?>
                                    </a>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                <?php }
                wp_reset_postdata(); ?>
                <div class="clr"></div>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
    <script type="text/javascript">
        jQuery(window).load(function(){
            <?php if($autoshowroom_detail_popup=='yes'){
            ?>
            jQuery('.cardeal_slide').lightGallery();
            <?php } ?>
            var slider_width = jQuery('#slider').width()-40;
            jQuery('#tab-description').addClass('in_active');
            var item_width = (slider_width/5);

            jQuery('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: item_width,
                itemMargin: 10,
                move:5,
                asNavFor: '#slider'
            });

            jQuery('#slider').flexslider({
                animation: "fade",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                smoothHeight: true,
                sync: "#carousel",
                directionNav: false,
                start: function(slider){
                    jQuery('body').removeClass('loading');
                }
            });

            var price_payment = jQuery('.autoshowroom-sidebar .pcd-pricing .pcd-price').text();

            jQuery('.payment-calculator .payment-price').attr('value',jQuery.trim(price_payment));
            var start_rate = jQuery('.down-pay-percent option:selected').attr('data-input');
            jQuery('.pay_rate').val(start_rate);
            jQuery('.down-pay-percent').on('change', function(){
                var rate = jQuery(this).find('option:selected').attr('data-input');
                jQuery('.pay_rate').val(rate);
            })

            jQuery('.payment_cal_btn').on('click', function(){
                var price_item = jQuery('.payment-price').val();
                var down_percent = jQuery('.down-pay-percent').val();
                var annual_percent = jQuery('.pay_rate').val();
                var period_time = jQuery('.down-pay-years').val();

                var price_number = price_item.replace(/[^0-9]/g, '');
                var down_pay_number = price_number*down_percent/100;

                var loan_pay = price_number - down_pay_number;
                var real_pay = loan_pay/period_time;
                var annual_pay_total = Math.round(((loan_pay*annual_percent)/100) * 100) / 100;
                var annual_pay = annual_pay_total/period_time;
                var total_pay = Math.round((parseInt(price_number) + annual_pay_total) * 100) / 100;

                var monthy_payment = Math.round((real_pay + annual_pay) * 100) / 100;

                jQuery('.monthy').html(monthy_payment);
                jQuery('.free-amount').html(annual_pay_total);
                jQuery('.total-pay').html(total_pay);
                jQuery('.payment_result').addClass('active');
            })
        });
    </script>

<?php get_template_part('template_inc/inc','contact'); ?>
<?php
get_footer();
